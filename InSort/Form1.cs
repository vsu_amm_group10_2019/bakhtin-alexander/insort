﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;

namespace InSort
{
    public partial class Form1 : Form
    {
		public int[] array;
		public char[] symbols;
		public Form1()
        {
            InitializeComponent();
			MaximizeBox = false;
			MaximizeBox = false;
			dataGridView1.RowCount = 10;
			dataGridView1.ColumnCount = 10;
			for (int i = 0; i < dataGridView1.Rows.Count; i++)
			{
				dataGridView1.Rows[i].HeaderCell.Value = i.ToString();
			}
			label6.Visible = false;
		}

        private void button1_Click_1(object sender, EventArgs e)
        {
			label6.Visible = true;
			label6.Refresh();
			Thread.Sleep(100);
			//сброс данных
			for (int i = 0; i < dataGridView1.Columns.Count; i++)
			{
				for (int j = 0; j < dataGridView1.Rows.Count; j++)
				{
					dataGridView1.Rows[j].Cells[i].Value = null;
				}
			}
			textBox4.Text = "";
			textBox5.Text = "";
			textBox6.Text = "";

			symbols = new char[int.Parse(textBox1.Text)];
			Random random = new Random();
			for (int i = 0; i < symbols.Length; ++i)
			{
				symbols[i] = (char)random.Next(char.Parse(textBox2.Text), char.Parse(textBox3.Text));				
			}
            for (int i = 0; i < symbols.Length; i++)
            {
				textBox5.Text += symbols[i]+" ";
			}

			array = new int[int.Parse(textBox1.Text)];
			int[] array1 = new int[10];
			int[] array2 = new int[10];
			for (int i = 0; i < array.Length; ++i)
			{
				array[i] = (int)symbols[i];
			}

            //Первичная сортировка
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 10 == 0)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (dataGridView1.Rows[0].Cells[j].Value == null)
						{
							dataGridView1.Rows[0].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
                    }

                }
				else if (array[i] % 10 == 1)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[1].Cells[j].Value == null)
						{
							dataGridView1.Rows[1].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 2)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[2].Cells[j].Value == null)
						{
							dataGridView1.Rows[2].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 3)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[3].Cells[j].Value == null)
						{
							dataGridView1.Rows[3].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 4)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[4].Cells[j].Value == null)
						{
							dataGridView1.Rows[4].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 5)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[5].Cells[j].Value == null)
						{
							dataGridView1.Rows[5].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 6)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[6].Cells[j].Value == null)
						{
							dataGridView1.Rows[6].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 7)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[7].Cells[j].Value == null)
						{
							dataGridView1.Rows[7].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 8)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[8].Cells[j].Value == null)
						{
							dataGridView1.Rows[8].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
				else if (array[i] % 10 == 9)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[9].Cells[j].Value == null)
						{
							dataGridView1.Rows[9].Cells[j].Value = array[i];
							dataGridView1.Refresh();
							Thread.Sleep(100);
							break;
						}
					}
				}
			}
			int n = 0;
			for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value!=null)
                    {
						array1[n] = int.Parse(dataGridView1.Rows[i].Cells[j].Value.ToString());
						n++;
						Thread.Sleep(100);
					}
                }
            }
            for (int i = 0; i < array1.Length; i++)
            {
				textBox6.Text += array1[i] + " ";
				textBox6.Refresh();
				Thread.Sleep(100);
			}
			//очистка ячеек
            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                for (int j = 0; j < dataGridView1.Rows.Count; j++)
                {
					dataGridView1.Rows[j].Cells[i].Value = null;
                }
            }
			textBox6.Text = "";
			//Конечная сортировка
			for (int i = 0; i < array1.Length; i++)
			{
				if (array1[i] / 10 == 0)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[0].Cells[j].Value == null)
						{
							dataGridView1.Rows[0].Cells[j].Value = array1[i];
							Thread.Sleep(100);
							dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 1)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[1].Cells[j].Value == null)
						{
							dataGridView1.Rows[1].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 2)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[2].Cells[j].Value == null)
						{
							dataGridView1.Rows[2].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 3)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[3].Cells[j].Value == null)
						{
							dataGridView1.Rows[3].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 4)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[4].Cells[j].Value == null)
						{
							dataGridView1.Rows[4].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 5)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[5].Cells[j].Value == null)
						{
							dataGridView1.Rows[5].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 6)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[6].Cells[j].Value == null)
						{
							dataGridView1.Rows[6].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 7)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[7].Cells[j].Value == null)
						{
							dataGridView1.Rows[7].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 8)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[8].Cells[j].Value == null)
						{
							dataGridView1.Rows[8].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
				else if (array1[i] / 10 == 9)
				{
					for (int j = 0; j < 10; j++)
					{
						if (dataGridView1.Rows[9].Cells[j].Value == null)
						{
							dataGridView1.Rows[9].Cells[j].Value = array1[i];
							Thread.Sleep(100); dataGridView1.Refresh();
							break;
						}
					}
				}
			}
			n = 0;
			for (int i = 0; i < dataGridView1.RowCount; i++)
			{
				for (int j = 0; j < dataGridView1.ColumnCount; j++)
				{
					if (dataGridView1.Rows[i].Cells[j].Value != null)
					{
						array2[n] = int.Parse(dataGridView1.Rows[i].Cells[j].Value.ToString());
						n++;
						Thread.Sleep(100);
					}
				}
			}
			for (int i = 0; i < array2.Length; i++)
			{
				textBox6.Text += array2[i] + " ";
				textBox6.Refresh();
				Thread.Sleep(100);
			}

			//показ начальной последовательности и конеченой
			Sort.sorting(array, array.Length, 3);		
			foreach (int x in array)
			{
				textBox4.Text+= Convert.ToChar(x) + " ";
			}

			label6.Visible = false;
		}

	}
}
